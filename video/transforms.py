from manim import *
import math
import numpy as np

class LinearOrNonlinear(Scene):
    def construct(self):
        grid = NumberPlane(
            x_range=[-2, 2, 0.1],
            y_range=[-2, 2, 0.1],
            x_length=20,
            y_length=20)

        def move(dur, eq):
            nonlocal grid, self

            grid.generate_target()
            grid.target.prepare_for_nonlinear_transform()
            grid.target.apply_function(eq)
            self.play(MoveToTarget(grid), run_time=dur)

        original = grid.copy()
        def restore(dur):
            self.play(Transform(grid, original), run_time=dur)

        title1 = Tex(r'\underline{Linear}', color=BLUE).scale(2).to_edge(UP)
        title2 = Tex(r'\underline{Non-linear}', color=RED).scale(2).to_edge(UP)

        title1.add_to_back(SurroundingRectangle(
            title1,
            color=BLACK,
            stroke_width=0,
            fill_opacity=1))

        title2.add_to_back(SurroundingRectangle(
            title2,
            color=BLACK,
            stroke_width=0,
            fill_opacity=1))

        dur = 2
        samt = 0.7

        grid.scale(1/samt)
        self.play(Create(grid), Create(title1))

        xamt = 1.0
        yamt = 1.0

        hamt = 0.6
        ramt = 45

        self.play(Rotate(grid, angle=0.5*ramt*DEGREES), run_time=dur)
        move(dur, lambda p: [p[0] + xamt, p[1] + yamt, p[2]])
        move(dur, lambda p: [samt*p[0] + hamt*p[1], p[1], p[2]])
        move(dur, lambda p: [p[0] - samt*xamt - hamt*yamt, p[1] - yamt, p[2]])
        move(dur, lambda p: [p[0] - hamt*p[1], -samt*p[1], p[2]])
        self.play(Rotate(grid, angle=-ramt*DEGREES), run_time=dur)

        self.play(ReplacementTransform(title1, title2))

        move(2*dur, lambda p: 2 * np.tanh(p))
        self.wait(1)
        restore(2*dur)

        self.wait(1)
        self.play(Uncreate(title2), Uncreate(grid))


